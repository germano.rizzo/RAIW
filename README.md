**BEWARE**: this project has been superseded by [Kluis](https://github.com/proofrock/Kluis). This one will not be developed anymore; it mostly works though, so it may still be useful.

# What is RAIW?

RAIW is an application that creates a sort of RAID system, but at file level; and exposes an FTP interface to store data. 

For now, it "implements" RAID1: allows to specify two directories on any filesystem, operating system, etc, and when you "write" something to the FTP interface, it gets encrypted, split, checksummed and written to those directories directories.

It's a lot more than RAID1, more like (part of) what btrfs/ZFS do, and it's at the file level. Data are secured while stored, it can be recovered after loss of one copy of the data, and tampering and bitrot can be detected. The rest of the storage is free to be used for other files besides the RAIW ones (provided you don't delete/modify its file structures).

## Details

 * Written in Kotlin - portable
 * Each file is split in blocks of 32MB (minus the checksum data - 16 bytes) and written to the directories
 * The encryption part uses:
   * CHACHA20/12 for encryption
   * BLAKE2b for hashing
   * Poly1305 for MAC, in encrypt-then-MAC mode
   * Scrypt for key derivation

## Roadmap
 
RAIW is very pre-alpha: assume that it WILL corrupt your data without warning! 

For the next release, it's planned to implement:

 * Offline scrubbing/filesystem check

For the first stable release:

 * Recover from checksum failures for data and metadata (using mirror), with automated repair
 * Offline scrubbing and repair
 * Stable on-disk format, with only backward-compatible changes from there on
 * Detection of low space

For the future (in this order of importance):

 * Mirroring of *n* copies of each block over *m* directories/drives, including 1 copy ("RAID0", but checksummed and encrypted)
 * Support of resume of writing and reading a file
 * Configurable block size and crypto algorithms
 * FTPS
 
 What it won't (probably) ever have:
 
  * RAID 5/6 or parity
  * Online scrubbing

## Status

Mostly working. Still TBD:

 * no scrubbing - no self healing
 * fails hard on tampering/bitrot
 * no support for resume both when reading or writing
 * no overwriting
 * something still needs to be optimized for the file formats.

## Try it!

Clone the repository, build a jar with the provided gradle build file.

create two empty directories, and initialize them

`java -jar RAIW-x.x.x.jar INIT --dir=dir1/ --dir=dir2/`

It will ask for a password of at least 8 chars, derive the key from it (thinking for a while), and exits.

Now, for any subsequent use, just start the FTP interface. 

`java -jar RAIW-x.x.x.jar FTP --dir=dir1/ --dir=dir2/ [--port=xxxx] [--ro]`

If a port is not specified with `--port=xxxx`, it will use the default, **1721**. The `--ro` switch opens the repo in read-only mode.

Use your favourite client to connect to host and specified port. The username is `raiw` and the password is the supplied one.

**BEWARE**: don't put anything important in it without having a "real" copy

**BEWARE**: ...no, really ;-)

## Usage

      java [-server] -jar CMD --dir=firstDir/ --dir=secondDir/

    CMD may be:

      INIT:   Initializes empty volumes

      IMPORT: Bulk imports a file or a tree in a RAIW directory, faster than
              using ftp and verifies that files are correctly transferred
        --from=xxx  Path of the file or directory to import
        --to=yyy    RAIW path in which to import

      FTP:    Opens a FTP server on the RAIW virtual filesystem
        --port=xxx  Port to use (default: 1722)
        --ro        (opt.) Opens the server in read-only mode

      SCRUB:  Performs an in-depth check of the integrity of the various
              metadata and data [FOR NOW READ-ONLY]

## License

RAIW is `(c) 2018- by Germano Rizzo`, licensed under the GPLv3
