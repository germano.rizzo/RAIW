package eu.germanorizzo.raiw

import com.guichaguri.minimalftp.api.ResponseException
import eu.germanorizzo.raiw.commands.*
import eu.germanorizzo.raiw.obj.AuthFailedException
import eu.germanorizzo.raiw.obj.DescFilesUtils
import eu.germanorizzo.raiw.obj.MetadataCorruptionException
import eu.germanorizzo.raiw.obj.RAIWFileSystem
import eu.germanorizzo.raiw.utils.Cache
import eu.germanorizzo.raiw.utils.ConsoleAppUtils
import eu.germanorizzo.raiw.utils.FLock
import java.nio.file.Path
import java.nio.file.Paths

class RAIW(private val dirs: Array<Path>, val ro: Boolean) {
    companion object {
        const val VOL_NUM: Byte = 2
        const val COPY_NUM: Byte = 2

        const val APP_NAME = "RAIW"
        const val APP_VERSION = "0.3.0"
        const val VERSION: Byte = 1
        const val FS_FILE = "$APP_NAME.fs"
    }

    private val cache = Cache<RAIWFileSystem>()

    fun auth(pwd: String): RAIWFileSystem? {
        val key =
                try {
                    DescFilesUtils.load(pwd, dirs)
                } catch (e: AuthFailedException) {
                    throw ResponseException(430, "Password seems to be wrong.")
                } catch (e: MetadataCorruptionException) {
                    throw ResponseException(551, "Metadata are different between the directories, or corrupted. Cannot continue.")
                }

        return cache.cached { RAIWFileSystem.load(dirs, key, ro) }
    }
}

val USAGE = """

    USAGE

      java [-server] -jar CMD --dir=firstDir/ --dir=secondDir/

    CMD may be:

      INIT:   Initializes empty volumes

      IMPORT: Bulk imports a file or a tree in a RAIW directory, faster than
              using ftp and verifies that files are correctly transferred
        --from=xxx  Path of the file or directory to import
        --to=yyy    RAIW path in which to import

      FTP:    Opens a FTP server on the RAIW virtual filesystem
        --port=xxx  Port to use (default: 1722)
        --ro        (opt.) Opens the server in read-only mode

      SCRUB:  Performs an in-depth check of the integrity of the various
              metadata and data [FOR NOW READ-ONLY]
""".trimIndent()

// Usage: java -jar RAIW.jar COMMAND --dir=dir1/ --dir=dir2/
// Where command can be:
//   INIT -> initialize empty dirs
//   FTP -> start an ftp on dirs
//      --port=2121 the port
//      --ro opens as read only
fun main(args: Array<String>) {
    println("${RAIW.APP_NAME} v.${RAIW.APP_VERSION}")

    if (args.size < 3)
        ConsoleAppUtils.abort("Too few arguments.", true)

    val dirs = args
            .filter { it.startsWith("--dir=") }
            .map { it.substring(6) }
            .map { Paths.get(it) }
            .toTypedArray()

    val fl = FLock(dirs)

    fl.flock {
        when (args[0].toUpperCase()) {
            INIT_CMD -> doInit(dirs)
            IMPORT_CMD -> doImport(args, dirs)
            SCRUB_CMD -> doScrub(dirs)
            FTP_CMD -> doFTP(args, dirs)
            else -> ConsoleAppUtils.abort("Command not understood", true)
        }
    }
}
