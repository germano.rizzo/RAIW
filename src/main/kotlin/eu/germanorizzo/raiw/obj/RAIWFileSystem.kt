package eu.germanorizzo.raiw.obj

import com.google.common.jimfs.Configuration
import com.google.common.jimfs.Feature
import com.google.common.jimfs.Jimfs
import com.google.common.jimfs.PathType
import com.guichaguri.minimalftp.api.IFileSystem
import com.guichaguri.minimalftp.api.ResponseException
import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.cypher.CypherInputStream
import eu.germanorizzo.raiw.cypher.CypherOutputStream
import eu.germanorizzo.raiw.utils.*
import java.io.*
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterInputStream

fun Path.getFileID() = Files.getAttribute(this, "basic:fileKey") as Int

class RAIWFileSystem private constructor(private val dirs: Array<Path>, val key: ByteArray, val ro: Boolean) : IFileSystem<Path> {
    companion object {
        private enum class COMMAND(val int: Int) {
            END(0), DIR_IN(1), DIR_UP(-1), FILE(2);

            val byte = int.toByte()

            companion object {
                fun byByte(b: Byte) = when (b) {
                    END.byte -> END
                    DIR_IN.byte -> DIR_IN
                    DIR_UP.byte -> DIR_UP
                    FILE.byte -> FILE
                    else -> ConsoleAppUtils.abort("Unknown command for value $b")
                }
            }
        }

        private class Walker(private val dos: DataOutputStream,
                             private val fileStructures: Map<Int, FileStructure>) : FileVisitor<Path> {
            override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
                if (dir?.parent != null)
                    dos.writeByte(COMMAND.DIR_UP.int)
                return FileVisitResult.CONTINUE
            }

            override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (file != null && file.fileName != null) {
                    dos.writeByte(COMMAND.FILE.int)
                    dos.writeUTF(file.fileName.toString())
                    fileStructures[file.getFileID()]!!.serialize(dos)
                }
                return FileVisitResult.CONTINUE
            }

            override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
                if (exc != null)
                    throw exc
                throw IOException()
            }

            override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                if (dir != null && dir.fileName != null) {
                    dos.writeByte(COMMAND.DIR_IN.int)
                    dos.writeUTF(dir.fileName.toString())
                }
                return FileVisitResult.CONTINUE
            }
        }

        fun load(dirs: Array<Path>, key: ByteArray, ro: Boolean): RAIWFileSystem {
            val dir = RndUtils.getRndElement(dirs)
            val file = dir.getSon(RAIW.FS_FILE)
            return if (!Files.exists(file)) {
                val fs = RAIWFileSystem(dirs, key, ro)
                fs.persist()
                fs
            } else {
                val ba = Files.readAllBytes(file)
                val baDec = CryptUtils.decrypt(ba, key)
                deserialize(baDec, dirs, key, ro)
            }
        }

        private fun deserialize(bs: ByteArray, dirs: Array<Path>, key: ByteArray, ro: Boolean) =
                RAIWFileSystem(dirs, key, ro).apply {
                    DataInputStream(InflaterInputStream(ByteArrayInputStream(bs))).use { ser ->
                        var cwd = rawFS.getPath("/")
                        mn@ while (true) {
                            val cmd = COMMAND.byByte(ser.readByte())
                            when (cmd) {
                                COMMAND.END ->
                                    break@mn
                                COMMAND.DIR_IN -> {
                                    cwd = rawFS.getPath(cwd.toString(), ser.readUTF())
                                    Files.createDirectories(cwd)
                                }
                                COMMAND.FILE -> {
                                    val f = Files.createFile(rawFS.getPath(cwd.toString(), ser.readUTF()))
                                    FileStructure.deserialize(ser, f).let { struct ->
                                        synchronized(fileNames) {
                                            fileStructures += f.getFileID() to struct
                                            fileNames += struct.fpn
                                        }
                                    }
                                }
                                COMMAND.DIR_UP ->
                                    cwd = cwd.parent
                            }
                        }
                    }
                }

        enum class CHECK_TYPE {
            FS_COHERENCE, FILES_INTEGRITY
        }

        enum class ERROR_TYPE {
            FILE_CHECKSUM_ERROR, FILE_NOT_REGISTERED, FILE_POINTER_DANGLING
        }
    }

    init {
        require(dirs.size == RAIW.COPY_NUM.toInt()) { "Must have ${RAIW.COPY_NUM} directories" }
    }

    private val s = Syncr()

    private val fileStructures: MutableMap<Int, FileStructure> = ConcurrentHashMap()
    private val fileNames: MutableSet<FilePathsAndName> = mutableSetOf()

    val rawFS: FileSystem = Jimfs.newFileSystem(
            Configuration.builder(PathType.unix())
                    .setRoots("/")
                    .setWorkingDirectory("/")
                    .setAttributeViews("basic")
                    .setSupportedFeatures(Feature.SECURE_DIRECTORY_STREAM, Feature.FILE_CHANNEL)
                    .build())
    private val rootDir = rawFS.getPath("/")

    val separator = rawFS.separator

    private fun checkReadOnly() {
        if (ro)
            throw ResponseException(503, "Filesystem is Read Only")
    }

    @Volatile
    var autoPersist = true

    override fun getRoot(): Path = rootDir

    override fun getPath(file: Path) = file.toAbsolutePath().toString()

    override fun exists(file: Path) = Files.exists(file)

    override fun isDirectory(file: Path) = Files.isDirectory(file)

    override fun getPermissions(file: Path) = if (ro) 292 else 438 // octal 0444 or 0666

    override fun getSize(file: Path) = fileStructures[file.getFileID()]?.size ?: Files.size(file)

    override fun getLastModified(file: Path) =
            fileStructures[file.getFileID()]?.lastModTime
                    ?: Files.getLastModifiedTime(file).toMillis()

    override fun getHardLinks(file: Path) = if (isDirectory(file)) 3 else 1

    override fun getName(file: Path) = file.fileName.toString()

    override fun getOwner(file: Path) = "raiw"

    override fun getGroup(file: Path) = "raiw"

    @Throws(IOException::class)
    override fun getParent(file: Path): Path =
            if (file == rootDir)
                throw FileNotFoundException("Cannot go upper than root")
            else
                file.parent

    @Throws(IOException::class)
    override fun listFiles(dir: Path): Array<Path> =
            if (!Files.isDirectory(dir))
                throw IOException("Not a directory")
            else
                Files.newDirectoryStream(dir).toCollection(mutableListOf<Path>()).toTypedArray()

    @Throws(IOException::class)
    override fun findFile(path: String): Path =
            rawFS.getPath(rootDir.toString(), path.replace('\\', '/')).also { file ->
                if (!isInside(rootDir, file))
                    throw FileNotFoundException("No permission to find this file (via root)")
            }

    @Throws(IOException::class)
    override fun findFile(cwd: Path, path: String): Path =
            rawFS.getPath(cwd.toString(), path.replace('\\', '/')).also { file ->
                if (!isInside(rootDir, file))
                    throw FileNotFoundException("No permission to find this file")
            }

    @Throws(IOException::class)
    override fun readFile(file: Path, start: Long): InputStream {
        try {
            if (start > 0)
                throw UnsupportedOperationException("File reading resume not implemented (yet?)")

            check(Files.exists(file)) { "File does not exist" }

            fileStructures[file.getFileID()]?.let {
                it.sync.lock()
                return CypherInputStream(dirs, it, key) { it.sync.unlock() }
            } ?: ConsoleAppUtils.abort("File does not have a matching structure")
        } catch (e: Throwable) {
            throw e
        }
    }

    @Throws(IOException::class)
    override fun writeFile(file: Path, start: Long): OutputStream {
        checkReadOnly()

        if (start > 0)
            throw UnsupportedOperationException("File writing resume/append not implemented (yet?)")

        val struct: FileStructure
        s.lock()
        try {
            if (Files.exists(file))
                throw UnsupportedOperationException("File overwriting not implemented (yet?)")

            Files.createFile(file)
            val id = file.getFileID()
            synchronized(fileNames) {
                struct = now().let { now -> FileStructure(file, getRndFilename(), 0L, now, now) }
                fileStructures += id to struct
            }
            persistNoSync()
        } finally {
            s.unlock()
        }

        struct.sync.lock()

        return CypherOutputStream(dirs, struct, key) { persist(); struct.sync.unlock() }
    }

    @Throws(IOException::class)
    override fun mkdirs(file: Path) {
        checkReadOnly()

        Files.createDirectories(file)
        persist()
    }

    @Throws(IOException::class)
    override fun delete(file: Path) {
        checkReadOnly()

        s.lock()
        try {
            val structsToRemove = mutableListOf<Int>()
            if (isDirectory(file))
                Files.walk(file)
                        .filter { !Files.isDirectory(it) }
                        .forEach { structsToRemove += it.getFileID() }
            else
                structsToRemove += file.getFileID()

            structsToRemove.forEach { idx ->
                fileStructures.remove(idx)?.let { rem ->
                    dirs.forEach { dir ->
                        val d = dir.getSon(rem.fpn.d1, rem.fpn.d2)
                        Files.walk(d)
                                .filter { it.fileName.toString().startsWith(rem.fpn.fn) }
                                .forEach { Files.delete(it) }
                    }
                    synchronized(fileNames) {
                        fileNames -= rem.fpn
                    }
                }
            }

            Files.walk(file).sorted(Comparator.reverseOrder()).forEach { Files.delete(it) }

            persistNoSync()
        } finally {
            s.unlock()
        }
    }

    @Throws(IOException::class)
    override fun rename(from: Path, to: Path) {
        checkReadOnly()

        val res = Files.move(from, to)
        fileStructures[res.getFileID()]?.file = res
        persist()
    }

    @Throws(IOException::class)
    override fun chmod(file: Path, perms: Int) {
        checkReadOnly()
        /* NOOP */
    }

    @Throws(IOException::class)
    override fun touch(file: Path, time: Long) {
        checkReadOnly()

        Files.setLastModifiedTime(file, FileTime.from(Instant.now()))
        persist()
    }

    private fun isInside(dir: Path, file: Path) =
            if (file == dir)
                true
            else
                try {
                    file.toAbsolutePath().toString().startsWith(dir.toAbsolutePath().toString())
                } catch (ex: IOException) {
                    false
                }

    private fun serialize(): ByteArray =
            ByteArrayOutputStream().use { baos ->
                DataOutputStream(DeflaterOutputStream(baos)).use {
                    Files.walkFileTree(rootDir, Walker(it, fileStructures))
                    it.writeByte(COMMAND.END.int)
                }

                baos.toByteArray()
            }

    private fun persist() {
        if (autoPersist)
            s.sync { persistNoSync() }
    }

    fun commit() {
        s.sync { commitNoSync() }
    }

    private fun persistNoSync() {
        if (autoPersist) {
            val ba = CryptUtils.encrypt(serialize(), key)
            dirs.forEach { dir ->
                Files.newOutputStream(dir.getSon(RAIW.FS_FILE)).use { it.write(ba) }
            }
        }
    }

    fun commitNoSync() {
        val ba = CryptUtils.encrypt(serialize(), key)
        dirs.forEach { dir ->
            Files.newOutputStream(dir.getSon(RAIW.FS_FILE)).use { it.write(ba) }
        }
    }

    private fun getRndFilename(): FilePathsAndName {
        synchronized(fileNames) {
            var ret: FilePathsAndName
            do {
                ret = FilePathsAndName(CryptUtils.getRndString(1),
                        CryptUtils.getRndString(1),
                        CryptUtils.getRndString(8))
            } while (ret in fileNames)
            fileNames += ret
            return ret
        }
    }

    fun scrub(key: ByteArray,
              onDirStart: (String) -> Unit,
              onCheckStart: (CHECK_TYPE) -> Unit,
              onProgress: (String, Int) -> Unit,
              onError: (ERROR_TYPE, String) -> Unit
    ) {
        fun checkLetters(str: String): Boolean {
            for (c in str)
                if (c != '.' && CryptUtils.c2i(c) == -1)
                    return false
            return true
        }

        s.sync {
            dirs.forEach { dir ->
                onDirStart(dir.toAbsolutePath().toString())
                onCheckStart(CHECK_TYPE.FS_COHERENCE)
                val realFileNames: MutableSet<FilePathsAndName> = mutableSetOf()
                Files.walk(dir)
                        .forEach {
                            if (Files.isRegularFile(it)) {
                                val d2 = it?.parent
                                val d1 = d2?.parent
                                val fn = it.fileName.toString().split('.')[0]
                                if (fn.length == 8 &&
                                        d1 != null) {
                                    val dfn1 = d1.fileName.toString()
                                    val dfn2 = d2.fileName.toString()
                                    if (dfn1.length == 1 &&
                                            dfn2.length == 1 &&
                                            checkLetters(dfn1) &&
                                            checkLetters(dfn2) &&
                                            checkLetters(fn))
                                        realFileNames += FilePathsAndName(dfn1, dfn2, fn)
                                }
                            }
                        }

                realFileNames
                        .filter { it !in fileNames }
                        .forEach { fn -> onError(ERROR_TYPE.FILE_NOT_REGISTERED, "${fn.d1}/${fn.d2}/${fn.fn}") }
                fileStructures.values
                        .filter { it.fpn !in realFileNames }
                        .filter { it.size > 0 }
                        .forEach { fn ->
                            onError(ERROR_TYPE.FILE_POINTER_DANGLING, fn.file.toString())
                        }

                onCheckStart(CHECK_TYPE.FILES_INTEGRITY)
                var i = 0
                fun percent(n: Int, t: Int): Int = Math.round(n.toDouble() / t * 100).toInt()
                fileStructures.forEach { _, fs ->
                    onProgress(fs.file.fileName.toString(), percent(i++, fileStructures.size))
                    CypherInputStream(dir, fs, key).use {
                        for (ii in 1..fs.size)
                            it.read()
                    }
                }
            }
        }
    }
}
