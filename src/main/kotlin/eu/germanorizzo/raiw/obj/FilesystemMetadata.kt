package eu.germanorizzo.raiw.obj

import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.cypher.BLOCK_SIZE
import eu.germanorizzo.raiw.utils.*
import eu.germanorizzo.raiw.utils.CryptUtils.KEY_SIZE
import eu.germanorizzo.raiw.utils.CryptUtils.MAC_SIZE
import eu.germanorizzo.raiw.utils.CryptUtils.SALT_SIZE
import eu.germanorizzo.raiw.utils.CryptUtils.update
import org.bouncycastle.crypto.params.KeyParameter
import org.bouncycastle.crypto.params.ParametersWithIV
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.nio.file.Files
import java.nio.file.Path

data class FilePathsAndName(val d1: String, val d2: String, val fn: String) {
    fun serialize(dos: DataOutputStream) {
        dos.writeUTF(d1)
        dos.writeUTF(d2)
        dos.writeUTF(fn)
    }

    companion object {
        fun deserialize(dis: DataInputStream) =
                FilePathsAndName(dis.readUTF(), dis.readUTF(), dis.readUTF())
    }
}

class FileStructure(
        @Volatile var file: Path,
        val fpn: FilePathsAndName,
        @Volatile var size: Long,
        @Volatile var creationTime: Long,
        @Volatile var lastModTime: Long
) {
    val sync = Syncr()

    val blocks: Int
        get() = Math.ceil(size.toDouble() / BLOCK_SIZE).toInt()

    fun serialize(dos: DataOutputStream) {
        fpn.serialize(dos)
        dos.writeLong(size)
        dos.writeLong(creationTime)
        dos.writeLong(lastModTime)
    }

    companion object {
        fun deserialize(dis: DataInputStream, file: Path) =
                FileStructure(
                        file,
                        FilePathsAndName.deserialize(dis),
                        dis.readLong(),
                        dis.readLong(),
                        dis.readLong()
                )
    }
}

object DescFilesUtils {
    const val FILE_NAME = "${RAIW.APP_NAME}.desc"
    private val APP_NAME_BYTES = "RAIW".toByteArray()
    private const val FMT_VER = 1.toByte()

    fun generate(password: String, dirs: Array<Path>): ByteArray {
        val salt = RndUtils.getRndByteArray(CryptUtils.SALT_SIZE)
        val pwd = CryptUtils.deriveKey(password, salt)
        val key = RndUtils.getRndByteArray(CryptUtils.KEY_SIZE)

        val encKey = ByteArray(CryptUtils.KEY_SIZE).also {
            CryptUtils.getCrypto().apply {
                init(true, ParametersWithIV(KeyParameter(pwd), salt))
                processBytes(key, 0, CryptUtils.KEY_SIZE, it, 0)
            }
        }

        val mac = ByteArray(CryptUtils.MAC_SIZE).also {
            CryptUtils.getMAC().apply {
                init(KeyParameter(key))
                update(APP_NAME_BYTES)
                update(FMT_VER)
                update(salt)
                update(encKey)
                doFinal(it, 0)
            }
        }

        val representation = ByteArrayOutputStream().apply {
            write(APP_NAME_BYTES)
            write(FMT_VER.toInt())
            write(salt)
            write(encKey)
            write(mac)
        }.toByteArray()

        dirs.forEach { dir ->
            Files.newOutputStream(dir.getSon(FILE_NAME)).use {
                it.write(representation, 0, representation.size)
            }
        }

        return key
    }

    @Throws(AuthFailedException::class, MetadataCorruptionException::class)
    fun load(password: String, dirs: Array<Path>): ByteArray {
        val (c1, c2) = dirs.map { it.getSon(FILE_NAME) }.map { Files.readAllBytes(it) }.toTypedArray()
        if (!ByteArrayUtils.equals(a = c1, b = c2))
            throw MetadataCorruptionException()

        fun ByteArray.split(vararg len: Int): Array<ByteArray> {
            val ret = mutableListOf<ByteArray>()
            ByteArrayInputStream(this).use { bais ->
                len.forEach { l ->
                    ByteArrayOutputStream().use { baos ->
                        for (i in 1..l)
                            baos.write(bais.read())
                        ret += baos.toByteArray()
                    }
                }
            }
            return ret.toTypedArray()
        }

        val (appName, fileFmtVer, salt, encKey, mac) =
                c1.split(APP_NAME_BYTES.size, 1, SALT_SIZE, KEY_SIZE, MAC_SIZE)

        if (!ByteArrayUtils.equals(a = appName, b = APP_NAME_BYTES) || fileFmtVer[0] != FMT_VER)
            throw MetadataCorruptionException()

        val pwd = CryptUtils.deriveKey(password, salt)

        val key = ByteArray(CryptUtils.KEY_SIZE).also {
            CryptUtils.getCrypto().apply {
                init(false, ParametersWithIV(KeyParameter(pwd), salt))
                processBytes(encKey, 0, CryptUtils.KEY_SIZE, it, 0)
            }
        }

        val mac2 = ByteArray(CryptUtils.MAC_SIZE).also {
            CryptUtils.getMAC().apply {
                init(KeyParameter(key))
                update(APP_NAME_BYTES)
                update(FMT_VER)
                update(salt)
                update(encKey)
                doFinal(it, 0)
            }
        }

        if (!ByteArrayUtils.equals(a = mac, b = mac2))
            throw AuthFailedException()

        return key
    }
}
