package eu.germanorizzo.raiw.obj

import com.guichaguri.minimalftp.FTPConnection
import com.guichaguri.minimalftp.api.IFileSystem
import com.guichaguri.minimalftp.api.IUserAuthenticator
import java.net.InetAddress
import java.nio.file.Path

class Authenticator(private val doAuth: (String) -> IFileSystem<Path>?) : IUserAuthenticator {
    override fun needsUsername(con: FTPConnection): Boolean {
        return true
    }

    override fun needsPassword(con: FTPConnection, username: String, address: InetAddress?): Boolean {
        return true
    }

    @Throws(IUserAuthenticator.AuthException::class)
    override fun authenticate(con: FTPConnection, address: InetAddress?, username: String?, password: String?): IFileSystem<Path> {
        if (username != "raiw" || password == null)
            throw IUserAuthenticator.AuthException()

        return doAuth(password) ?: throw IUserAuthenticator.AuthException()
    }
}
