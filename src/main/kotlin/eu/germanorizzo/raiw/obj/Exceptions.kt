package eu.germanorizzo.raiw.obj

class AuthFailedException : Exception()

class MetadataCorruptionException : Exception()