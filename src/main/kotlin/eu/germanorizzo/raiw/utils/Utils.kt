package eu.germanorizzo.raiw.utils

import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.USAGE
import org.bouncycastle.crypto.Digest
import org.bouncycastle.crypto.Mac
import org.bouncycastle.crypto.StreamCipher
import org.bouncycastle.crypto.digests.Blake2bDigest
import org.bouncycastle.crypto.engines.ChaChaEngine
import org.bouncycastle.crypto.generators.SCrypt
import org.bouncycastle.crypto.macs.Poly1305
import org.bouncycastle.crypto.params.KeyParameter
import org.bouncycastle.crypto.params.ParametersWithIV
import java.io.*
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.nio.file.Paths
import java.security.SecureRandom
import java.util.concurrent.Semaphore
import kotlin.system.exitProcess

fun now() = System.currentTimeMillis()

fun Path.getSon(vararg more: String): Path = Paths.get(this.toString(), *more)

fun InputStream.readByte(): Byte = this.read().toByte()

fun <T> timed(what: () -> T): T {
    val a = now()
    try {
        return what()
    } finally {
        println(">" + (now() - a) + " millis")
    }
}

class Cache<T> {
    private var t: T? = null

    @Synchronized
    fun cached(getter: () -> T) = if (t == null) {
        t = getter()
        t!!
    } else t!!
}

object RndUtils {
    private val RND = SecureRandom()

    @Synchronized
    fun getRndByteArray(len: Int) = ByteArray(len).also { RND.nextBytes(it) }

    @Synchronized
    fun getRndByte() = getRndInt().toByte()

    @Synchronized
    fun getRndInt() = RND.nextInt()

    @Synchronized
    fun getRndInt(limit: Int) = RND.nextInt(limit)

    @Synchronized
    fun <T> getRndElement(arr: List<T>): T = arr[RND.nextInt(arr.size)]

    @Synchronized
    fun getRndElement(arr: ByteArray): Byte = arr[RND.nextInt(arr.size)]

    @Synchronized
    fun <K, V> getRndElement(map: Map<K, V>): Pair<K, V> {
        val keys = map.keys.toList()
        val key = getRndElement(keys)
        return key to map[key]!!
    }

    @Synchronized
    fun <T> getRndElement(arr: Array<T>): T = arr[RND.nextInt(arr.size)]
}

object CryptUtils {
    private const val SER = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private const val SER_SIZE = SER.length

    fun i2s(i: Int) =
            StringBuilder().also { sb ->
                fun myi2s(sb: StringBuilder, i: Int) {
                    if (i >= SER_SIZE)
                        myi2s(sb, i / SER_SIZE)
                    sb.append(SER[i % SER_SIZE])
                }
                myi2s(sb, i)
            }.toString()

    fun s2i(s: String): Int {
        var i = 0
        s.toCharArray().forEach { c ->
            i *= SER_SIZE
            i += c2i(c)
        }
        return i
    }

    fun c2i(c: Char) = SER.indexOf(c)

    fun getRndString(len: Int) =
            StringBuilder().also { sb ->
                for (i in 1..len)
                    sb.append(CryptUtils.SER[RndUtils.getRndInt(CryptUtils.SER_SIZE)])
            }.toString()

    const val SALT_SIZE = 8
    const val IV_SIZE = 8
    const val KEY_SIZE = 256 / 8
    val MAC_SIZE = getMAC().macSize
    val HASH_SIZE = getHash().digestSize

    fun getCrypto(): StreamCipher = ChaChaEngine(12)
    fun getMAC(): Mac = Poly1305()
    fun getHash(): Digest = Blake2bDigest()

    fun Mac.update(ba: ByteArray) {
        this.update(ba, 0, ba.size)
    }

    fun deriveKey(pass: String, salt: ByteArray): ByteArray {
        val pb = pass.toByteArray(StandardCharsets.UTF_8)
        return SCrypt.generate(pb, salt, 131072, 8, 1, KEY_SIZE)
    }

    fun encrypt(what: ByteArray, key: ByteArray): ByteArray {
        require(key.size == KEY_SIZE) { "Key must be $KEY_SIZE bytes long" }

        val ivp = ParametersWithIV(KeyParameter(key), RndUtils.getRndByteArray(IV_SIZE))

        val ret = ByteArray(what.size + MAC_SIZE + IV_SIZE)
        ByteArrayUtils.copy(ivp.iv, 0, ret, 0, IV_SIZE)

        val cypher = getCrypto()
        cypher.init(true, ivp)
        cypher.processBytes(what, 0, what.size, ret, IV_SIZE)

        val mac = getMAC()
        mac.init(ivp.parameters)
        mac.update(ret, IV_SIZE, what.size)
        mac.doFinal(ret, IV_SIZE + what.size)

        return ret
    }

    fun decrypt(what: ByteArray,
                key: ByteArray,
                onIntegrityCheckFailed: () -> Exception = { IllegalStateException("Integrity check failed") }): ByteArray {
        require(key.size == KEY_SIZE) { "Key must be $KEY_SIZE bytes long" }
        require(what.size >= IV_SIZE + MAC_SIZE) { "Argument to decrypt is too short" }

        val ivp = ParametersWithIV(KeyParameter(key), what, 0, IV_SIZE)

        val macba = ByteArray(MAC_SIZE)
        val mac = getMAC()
        mac.init(ivp.parameters)
        mac.update(what, IV_SIZE, what.size - MAC_SIZE - IV_SIZE)
        mac.doFinal(macba, 0)

        if (!ByteArrayUtils.equals(macba, 0, what, what.size - MAC_SIZE, MAC_SIZE))
            throw onIntegrityCheckFailed()

        val ret = ByteArray(what.size - MAC_SIZE - IV_SIZE)

        val cypher = getCrypto()
        cypher.init(false, ivp)
        cypher.processBytes(what, IV_SIZE, ret.size, ret, 0)

        return ret
    }
}

object ByteArrayUtils {
    fun equals(a: ByteArray, offA: Int = 0, b: ByteArray, offB: Int = 0, len: Int = a.size): Boolean {
        require(offA >= 0) { "Offset must be positive" }
        require(offB >= 0) { "Offset must be positive" }
        for (i in 0 until len)
            try {
                if (a[offA + i] != b[offB + i])
                    return false
            } catch (e: ArrayIndexOutOfBoundsException) {
                return false
            }
        return true
    }

    fun copy(from: ByteArray, fromWhere: Int, to: ByteArray, toWhere: Int, len: Int) {
        for (i in 0 until len)
            to[toWhere + i] = from[fromWhere + i]
    }
}

class Syncr {
    private val lock = Semaphore(1)

    fun lock() {
        // println("A> ${Exception().stackTrace[2]}")
        lock.acquire()
    }

    fun unlock() {
        lock.drainPermits()
        lock.release()
        // println("R> ${Exception().stackTrace[2]}")
    }

    fun <T> sync(block: () -> T): T {
        lock()
        try {
            return block()
        } finally {
            unlock()
        }
    }
}

object ConsoleAppUtils {
    fun askString(prompt: String): String {
        print("$prompt: ")
        return BufferedReader(InputStreamReader(System.`in`)).readLine()
    }

    fun askPassword(prompt: String = "Enter password"): String {
        val c = System.console() ?: return askString(prompt)
        return String(c.readPassword("$prompt: "))
    }

    fun askConfirmation(prompt: String): Boolean {
        print("$prompt ")
        while (true) {
            when (askString("(Y or N)")) {
                "Y", "y" -> return true
                "N", "n" -> return false
            }
            println()
        }
    }

    fun abort(err: String, withUsage: Boolean = false): Nothing {
        System.err.println(err)
        if (withUsage)
            System.err.println(USAGE)
        exitProcess(-1)
    }
}

class FLock(private val dirs: Array<Path>) {
    companion object {
        const val LOCK_FILE = "${RAIW.APP_NAME}.lock"

        private class Poker
        private constructor(private val f: File,
                            private val fos: FileOutputStream,
                            private val fc: FileChannel,
                            private val l: FileLock?) : Closeable {
            companion object {
                private fun noEx(block: () -> Unit) {
                    try {
                        block.invoke()
                    } catch (e: java.lang.Exception) {
                    }
                }

                fun get(dir: Path): Poker {
                    val f = File(dir.toString(), LOCK_FILE)
                    val fos = FileOutputStream(f)
                    val fc = fos.channel
                    val l: FileLock? = fc.tryLock()
                    return Poker(f, fos, fc, l)
                }
            }

            val ok: Boolean
                get() = l != null

            override fun close() {
                noEx { l?.release() }
                noEx { fc.close() }
                noEx { fos.close() }
                noEx { f.delete() }
            }
        }
    }

    fun flock(block: () -> Unit) {
        val pokers = dirs.map { Poker.get(it) }
        try {
            if (pokers.any { !it.ok })
                ConsoleAppUtils.abort("Another instance seems to be in use. Aborting.")
            block.invoke()
        } finally {
            pokers.forEach { it.close() }
        }
    }
}