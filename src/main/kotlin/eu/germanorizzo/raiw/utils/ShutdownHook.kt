package eu.germanorizzo.raiw.utils

fun addShutdownHook(sh: () -> Unit) {
    val hook = Thread { sh() }
    val rt = Runtime.getRuntime()
    rt.addShutdownHook(hook)
}