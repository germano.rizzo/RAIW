package eu.germanorizzo.raiw.commands

import com.guichaguri.minimalftp.api.ResponseException
import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.obj.RAIWFileSystem
import eu.germanorizzo.raiw.utils.ByteArrayUtils
import eu.germanorizzo.raiw.utils.ConsoleAppUtils
import eu.germanorizzo.raiw.utils.CryptUtils
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

const val IMPORT_CMD = "IMPORT"

fun doImport(args: Array<String>, dirs: Array<Path>) {
    require(dirs.size == RAIW.VOL_NUM.toInt()) { "${RAIW.VOL_NUM} directories must be specified" }

    require(Files.isDirectory(dirs[0])) { "The first dir is not valid" }
    require(Files.isDirectory(dirs[1])) { "The second dir is not valid" }

    val from =
            Paths.get(args.firstOrNull { it.startsWith("--from=") }
                    ?.substring(7)
                    ?: ConsoleAppUtils.abort("Please specify source with --from=", true))

    require(Files.exists(from)) { "The source is not valid" }

    val pwd = ConsoleAppUtils.askPassword("Enter password")

    val raiw = RAIW(dirs, false)

    val fs =
            try {
                raiw.auth(pwd)
                        ?: ConsoleAppUtils.abort("Error in loading filesystem")
            } catch (e: ResponseException) {
                ConsoleAppUtils.abort(e.message ?: "")
            }

    fs.autoPersist = false

    val to =
            fs.rawFS.getPath(args.firstOrNull { it.startsWith("--to=") }
                    ?.substring(5)
                    ?: ConsoleAppUtils.abort("Please specify destination dir with --to=", true))

    require(Files.isDirectory(to)) { "The destination dir is not valid" }

    if (!fs.exists(to)) {
        if (!ConsoleAppUtils.askConfirmation("Destination directory does not exists. Want me to create it?"))
            ConsoleAppUtils.abort("Ok. Bye!")
        fs.mkdirs(to)
    }

    if (Files.isRegularFile(from))
        copyFile(from, to, fs)
    else
        copyDir(from, to, fs)

    print("All done. Persisting filesystem... ")
    fs.commit()
    println("Ok.")
}

private fun copyFile(from: Path, to: Path, fs: RAIWFileSystem, num: Int = -1, tot: Int = 1) {
    fun percent(n: Int, t: Int): Int = Math.round(n.toDouble() / t * 100).toInt()

    val fname = from.fileName.toString()
    print("\r")
    if (num >= 0)
        print("[${percent(num, tot)}%] ")
    print("Copying $fname... ")

    val dest = fs.rawFS.getPath(to.toAbsolutePath().toString(), fname)
    if (Files.exists(dest)) {
        println()
        if (!ConsoleAppUtils.askConfirmation("The file already exists. Do you want to overwrite it?"))
            return
        fs.delete(dest) // TODO commit after this?
    }

    val hhFrom = ByteArray(size = CryptUtils.HASH_SIZE)
    Files.newInputStream(from).use { iis ->
        val hashFrom = CryptUtils.getHash()
        fs.writeFile(dest, 0).use { os ->
            val buffer = ByteArray(64 * 1024)
            var bytes = iis.read(buffer)
            while (bytes >= 0) {
                os.write(buffer, 0, bytes)
                hashFrom.update(buffer, 0, bytes)
                bytes = iis.read(buffer)
            }
        }
        hashFrom.doFinal(hhFrom, 0)
    }

    val hhTo = ByteArray(CryptUtils.HASH_SIZE)
    fs.readFile(dest, 0).use { iis ->
        val hashTo = CryptUtils.getHash()
        val buffer = ByteArray(64 * 1024)
        var bytes = iis.read(buffer)
        while (bytes >= 0) {
            hashTo.update(buffer, 0, bytes)
            bytes = iis.read(buffer)
        }
        hashTo.doFinal(hhTo, 0)
    }
    if (!ByteArrayUtils.equals(a = hhFrom, b = hhTo)) {
        //TODO don't retry indefinitely
        println("Error in verifying $dest. Retrying...")
        fs.delete(dest)
        copyFile(from, to, fs)
    }



    print("Ok.                                        ")
}

fun copyDir(from: Path, to: Path, fs: RAIWFileSystem) {
    val fname = from.fileName.toString()
    println("\rCopying directory tree $fname... ")

    val dest = fs.rawFS.getPath(to.toAbsolutePath().toString(), fname)
    if (Files.exists(dest)) {
        // TODO merge/overwrite?
        ConsoleAppUtils.abort("The directory already exists. Cannot proceed.")
    }

    println("Duplicating directory tree...")
    val filesToCopy = mutableMapOf<String, Path>()
    var cwd = to

    class Walker : FileVisitor<Path> {
        override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
            if (dir?.parent != null)
                cwd = cwd.parent
            return FileVisitResult.CONTINUE
        }

        override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
            if (file != null && file.fileName != null) {
                filesToCopy +=
                        from.relativize(file).toString() to
                        fs.rawFS.getPath(to.relativize(cwd).toString(), file.fileName.toString())
            }
            return FileVisitResult.CONTINUE
        }

        override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
            if (exc != null)
                throw exc
            throw IOException()
        }

        override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
            if (dir != null && dir.fileName != null) {
                val newCwd = fs.rawFS.getPath(cwd.toString(), dir.fileName.toString())
                Files.createDirectories(newCwd)
                cwd = newCwd
            }
            return FileVisitResult.CONTINUE
        }
    }

    Files.walkFileTree(from, Walker())

    println("Duplicating ${filesToCopy.size} files...")

    var i = 0
    filesToCopy.forEach { f, t -> copyFile(Paths.get(from.toString(), f), t.parent, fs, i++, filesToCopy.size) }
}
