package eu.germanorizzo.raiw.commands

import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.obj.DescFilesUtils
import eu.germanorizzo.raiw.utils.ConsoleAppUtils
import eu.germanorizzo.raiw.utils.FLock
import java.nio.file.Files
import java.nio.file.Path

const val INIT_CMD = "INIT"

fun doInit(dirs: Array<Path>) {
    require(dirs.size == RAIW.VOL_NUM.toInt()) { "${RAIW.VOL_NUM} directories must be specified" }

    require(Files.isDirectory(dirs[0])) { "The first dir is not valid" }
    require(Files.isDirectory(dirs[1])) { "The second dir is not valid" }

    val isEmptyD1 = Files.walk(dirs[0]).filter { it.fileName.toString() != FLock.LOCK_FILE }.count() == 1L
    val isEmptyD2 = Files.walk(dirs[1]).filter { it.fileName.toString() != FLock.LOCK_FILE }.count() == 1L

    if (isEmptyD1 && isEmptyD2) {
        // Two uninitialized directories
        val pass = ConsoleAppUtils.askPassword("Initializing. Enter a password (at least 8 chars)")
        if (pass.length < 8)
            ConsoleAppUtils.abort("Password too short. Aborting.")

        DescFilesUtils.generate(pass, dirs)

        println("Directories correctly initialized.")
    } else
        ConsoleAppUtils.abort("At least one of the specified directory is not empty. Aborting.")
}