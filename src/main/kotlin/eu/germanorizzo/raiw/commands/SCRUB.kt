package eu.germanorizzo.raiw.commands

import com.guichaguri.minimalftp.api.ResponseException
import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.obj.RAIWFileSystem
import eu.germanorizzo.raiw.utils.ConsoleAppUtils
import java.nio.file.Path

const val SCRUB_CMD = "SCRUB"

fun doScrub(dirs: Array<Path>) {
    require(dirs.size == RAIW.VOL_NUM.toInt()) { "${RAIW.VOL_NUM} directories must be specified" }

    val pwd = ConsoleAppUtils.askPassword("Enter password")

    print("Loading structures...")
    val raiw = RAIW(dirs, true)
    val fs =
            try {
                raiw.auth(pwd)
                        ?: ConsoleAppUtils.abort("Error in loading filesystem")
            } catch (e: ResponseException) {
                ConsoleAppUtils.abort(e.message ?: "")
            }
    println("Ok. Now starting the check.")
    var ok = true
    fs.scrub(fs.key,
            { dir -> println(); println("Scrubbing dir $dir...") },
            { check ->
                println("Starting check on ${
                when (check) {
                    RAIWFileSystem.Companion.CHECK_TYPE.FS_COHERENCE -> "coherence of virtual and real filesystems"
                    RAIWFileSystem.Companion.CHECK_TYPE.FILES_INTEGRITY -> "files integrity"
                }
                }...")
            },
            { file, percent -> print("\r$percent% Now checking $file                                             ") },
            { type, err -> println(); println("ERROR: $type, $err"); ok = false }
    )

    if (ok)
        println("All OK.")
    else
        println("Error(s) occourred. Please review the logs.")
}
