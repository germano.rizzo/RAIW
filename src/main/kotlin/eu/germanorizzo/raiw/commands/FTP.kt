package eu.germanorizzo.raiw.commands

import com.guichaguri.minimalftp.FTPServer
import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.obj.Authenticator
import eu.germanorizzo.raiw.utils.addShutdownHook
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.CountDownLatch

const val FTP_CMD = "FTP"

private const val DEFAULT_PORT = 1721

private val cdl = CountDownLatch(1)

fun doFTP(args: Array<String>, dirs: Array<Path>) {
    addShutdownHook {
        cdl.countDown()
    }

    var port = DEFAULT_PORT
    args.firstOrNull { it.startsWith("--port=") }?.let { port = it.substring(7).toInt() }
    val ro = "--ro" in args
    require(dirs.size == RAIW.VOL_NUM.toInt()) { "${RAIW.VOL_NUM} directories must be specified" }

    require(Files.isDirectory(dirs[0])) { "The first dir is not valid" }
    require(Files.isDirectory(dirs[1])) { "The second dir is not valid" }

    val raiw = RAIW(dirs, ro)

    val readonly = if (ro) " Read Only" else ""
    print("Starting$readonly FTP Server on port $port... ")
    val ftp = FTPServer(Authenticator(raiw::auth))
    ftp.listen(port)
    println("Ok!")

    ftp.use {
        cdl.await() // blocks
    }
}
