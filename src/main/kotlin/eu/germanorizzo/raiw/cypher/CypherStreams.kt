package eu.germanorizzo.raiw.cypher

import eu.germanorizzo.raiw.RAIW
import eu.germanorizzo.raiw.obj.FilePathsAndName
import eu.germanorizzo.raiw.obj.FileStructure
import eu.germanorizzo.raiw.utils.CryptUtils
import eu.germanorizzo.raiw.utils.RndUtils
import eu.germanorizzo.raiw.utils.getSon
import eu.germanorizzo.raiw.utils.now
import org.bouncycastle.crypto.params.KeyParameter
import org.bouncycastle.crypto.params.ParametersWithIV
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.util.*

const val BLOCK_SIZE = 32 * 1024 * 1024 //32Mb
private val EFFECTIVE_SIZE = BLOCK_SIZE - CryptUtils.MAC_SIZE

private fun calcExtension(blknum: Int) = CryptUtils.i2s(blknum).padStart(3, '0')

private fun getIV(fpn: FilePathsAndName, ext: String) = ByteArray(CryptUtils.HASH_SIZE).also {
    val hash = CryptUtils.getHash()
    fpn.d1.toByteArray(StandardCharsets.US_ASCII).let { ba -> hash.update(ba, 0, ba.size) }
    fpn.d2.toByteArray(StandardCharsets.US_ASCII).let { ba -> hash.update(ba, 0, ba.size) }
    fpn.fn.toByteArray(StandardCharsets.US_ASCII).let { ba -> hash.update(ba, 0, ba.size) }
    ext.toByteArray(StandardCharsets.US_ASCII).let { ba -> hash.update(ba, 0, ba.size) }
    hash.doFinal(it, 0)
}.copyOf(CryptUtils.IV_SIZE)

class CypherOutputStream(private val dirs: Array<Path>,
                         private val file: FileStructure,
                         key: ByteArray,
                         private val onClose: () -> Unit = {}) : OutputStream() {
    init {
        require(dirs.size == RAIW.VOL_NUM.toInt())
        require(key.size == CryptUtils.KEY_SIZE)
    }

    private val cypher = CryptUtils.getCrypto()
    private val mac = CryptUtils.getMAC()

    private val kp = KeyParameter(key, 0, key.size)

    private val block = ByteArray(EFFECTIVE_SIZE)
    private val blockTo = ByteArray(BLOCK_SIZE)
    private var pntr = 0
    private var blknum = 0

    private var written = 0L

    override fun write(b: Int) {
        block[pntr] = (b and 0xFF).toByte()
        pntr++
        if (pntr == EFFECTIVE_SIZE)
            flushBlock()
    }

    private fun flushBlock() {
        val ext = calcExtension(blknum)
        val iv = getIV(file.fpn, ext)
        val ivp = ParametersWithIV(kp, iv, 0, iv.size)

        cypher.init(true, ivp)

        cypher.processBytes(block, 0, pntr, blockTo, 0)

        mac.init(kp)
        mac.update(blockTo, 0, pntr)
        mac.doFinal(blockTo, pntr)

        fun writeToFile(dir: Path, name: FilePathsAndName, what: ByteArray) {
            val d1 = dir.getSon(name.d1)
            val d2 = d1.getSon(name.d2)
            Files.createDirectories(d2)
            Files.newOutputStream(d2.getSon("${name.fn}.$ext")).use {
                it.write(what, 0, pntr + CryptUtils.MAC_SIZE)
            }
        }

        dirs.forEach { writeToFile(it, file.fpn, blockTo) }

        written += pntr
        pntr = 0
        blknum++
        file.lastModTime = now()
        file.size = written
    }

    override fun flush() {
    }

    override fun close() {
        if (pntr > 0)
            flushBlock()
        onClose()
    }
}

class CypherInputStream(private val dir: Path,
                        private val file: FileStructure,
                        key: ByteArray,
                        private val onClose: () -> Unit = {}) : InputStream() {
    constructor(dirs: Array<Path>,
                file: FileStructure,
                key: ByteArray,
                onClose: () -> Unit = {}) : this(RndUtils.getRndElement(dirs), file, key, onClose)

    init {
        require(key.size == CryptUtils.KEY_SIZE)
    }

    private val cypher = CryptUtils.getCrypto()
    private val mac = CryptUtils.getMAC()

    private val kp = KeyParameter(key, 0, key.size)

    private val block = ByteArray(EFFECTIVE_SIZE)
    private val blockTo = ByteArray(EFFECTIVE_SIZE)

    private var readBytes = 0L
    private val readBytesInBlock
        get() = (readBytes % EFFECTIVE_SIZE).toInt()
    private val blockNum
        get() = (readBytes / EFFECTIVE_SIZE).toInt()
    private val available
        get() = file.size - readBytes

    private fun loadBlock(len: Int) {
        require(len <= EFFECTIVE_SIZE)

        val ext = calcExtension(blockNum)
        val macBlock = ByteArray(CryptUtils.MAC_SIZE)
        Files.newInputStream(dir.getSon(file.fpn.d1, file.fpn.d2, "${file.fpn.fn}.$ext")).use {
            it.read(block, 0, len)
            it.read(macBlock, 0, CryptUtils.MAC_SIZE)
        }

        val iv = getIV(file.fpn, ext)
        val ivp = ParametersWithIV(kp, iv, 0, iv.size)

        val macBlock2 = ByteArray(CryptUtils.MAC_SIZE)
        mac.init(kp)
        mac.update(block, 0, len)
        mac.doFinal(macBlock2, 0)

        check(Arrays.equals(macBlock, macBlock2)) { "Decryption failed for ${file.fpn}, block $ext" }


        cypher.init(false, ivp)

        cypher.processBytes(block, 0, len, blockTo, 0)
    }

    override fun read(): Int {
        if (available == 0L)
            return -1

        fun cropToEffSize(v: Long) = if (v >= EFFECTIVE_SIZE) EFFECTIVE_SIZE else v.toInt()

        if (readBytes % EFFECTIVE_SIZE == 0L)
            loadBlock(cropToEffSize(available))

        // TODO optimize?
        val ret = blockTo[readBytesInBlock].toInt() and 0xFF
        readBytes++
        return ret
    }

    override fun available() = if (available > Int.MAX_VALUE) Int.MAX_VALUE else available.toInt()

    override fun close() {
        onClose()
    }
}
