package eu.germanorizzo.raiw

import com.guichaguri.minimalftp.api.IFileSystem
import eu.germanorizzo.raiw.obj.RAIWFileSystem
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Path

class LoggingFS(val wrapped: RAIWFileSystem) : IFileSystem<Path> {
    init {
        println("INIT")
        dump()
    }

    override fun getGroup(file: Path): String {
        println("getGroup(\"$file\")")
        dump()
        return wrapped.getGroup(file)
    }

    override fun getName(file: Path): String {
        println("getName(\"$file\")")
        dump()
        val ret = wrapped.getName(file)
        dump()
        return ret
    }

    override fun getSize(file: Path) = wrapped.getSize(file)
            .also { l: Long -> println("getSize(\"$file\") = $l"); dump() }

    override fun getPermissions(file: Path): Int {
        println("getPermissions(\"$file\")")
        dump()
        return wrapped.getPermissions(file)
    }

    override fun mkdirs(file: Path) {
        println("mkdirs(\"$file\")")
        dump()
        wrapped.mkdirs(file)
    }

    override fun getHardLinks(file: Path): Int {
        println("getHardLinks(\"$file\")")
        dump()
        return wrapped.getHardLinks(file)
    }

    override fun listFiles(dir: Path): Array<Path> {
        println("listFiles(\"$dir\")")
        dump()
        return wrapped.listFiles(dir)
    }

    override fun writeFile(file: Path, start: Long): OutputStream {
        println("writeFile(\"$file\")")
        dump()
        return wrapped.writeFile(file, start)
    }

    override fun rename(from: Path, to: Path) {
        println("rename(\"$from\", \"$to\")")
        dump()
        wrapped.rename(from, to)
    }

    override fun delete(file: Path) {
        println("delete(\"$file\")")
        dump()
        wrapped.delete(file)
    }

    override fun getOwner(file: Path): String {
        println("getOwner(\"$file\")")
        dump()
        return wrapped.getOwner(file)
    }

    override fun getParent(file: Path): Path {
        println("getParent(\"$file\")")
        dump()
        return wrapped.getParent(file)
    }

    override fun touch(file: Path, time: Long) {
        println("touch(\"$file\", $time)")
        dump()
        wrapped.touch(file, time)
    }

    override fun getRoot(): Path {
        println("getRoot()")
        dump()
        return wrapped.root
    }

    override fun getPath(file: Path): String {
        println("getPath(\"$file\")")
        dump()
        return wrapped.getPath(file)
    }

    override fun readFile(file: Path, start: Long): InputStream {
        println("readFile(\"$file\")")
        dump()
        return wrapped.readFile(file, start)
    }

    override fun isDirectory(file: Path): Boolean {
        println("isDirectory(\"$file\")")
        dump()
        return wrapped.isDirectory(file)
    }

    override fun findFile(path: String): Path {
        println("findFile(\"$path\")")
        dump()
        return wrapped.findFile(path)
    }

    override fun findFile(cwd: Path, path: String): Path {
        println("findFile(\"$cwd\", \"$path\")")
        dump()

        val ret = wrapped.findFile(cwd, path)

        return ret
    }

    override fun getLastModified(file: Path): Long {
        println("getLastModified(\"$file\")")
        dump()
        return wrapped.getLastModified(file)
    }

    override fun exists(file: Path): Boolean {
        println("exists(\"$file\")")
        dump()
        return wrapped.exists(file)
    }

    override fun chmod(file: Path, perms: Int) {
        println("chmod(\"$file\", $perms)")
        dump()
        wrapped.chmod(file, perms)
    }

    private fun dump() {
        print(" >")
        wrapped.listFiles(wrapped.root).forEach { print(">" + it) }
        println()
    }
}