package eu.germanorizzo.raiw

import eu.germanorizzo.raiw.cypher.BLOCK_SIZE
import eu.germanorizzo.raiw.cypher.CypherInputStream
import eu.germanorizzo.raiw.cypher.CypherOutputStream
import eu.germanorizzo.raiw.obj.FilePathsAndName
import eu.germanorizzo.raiw.obj.FileStructure
import eu.germanorizzo.raiw.utils.ByteArrayUtils
import eu.germanorizzo.raiw.utils.CryptUtils
import eu.germanorizzo.raiw.utils.RndUtils
import java.nio.file.Path
import java.nio.file.Paths

private const val TEST_LEN = BLOCK_SIZE * 3 + 10285

fun main(args: Array<String>) {
    print("Testing Cypher Streams... ")
    val dirs = args.map { Paths.get(it) }.toTypedArray()

    fun getRndFilename() =
            FilePathsAndName(CryptUtils.getRndString(1),
                    CryptUtils.getRndString(1),
                    CryptUtils.getRndString(8))

    val f = FileStructure(Paths.get("/"), getRndFilename(), 0L, 0L, 0L)
    val k = RndUtils.getRndByteArray(256 / 8)

    val blk = RndUtils.getRndByteArray(TEST_LEN)

    CypherOutputStream(dirs, f, k).use { os ->
        os.write(blk)
    }

    val blk2 = ByteArray(TEST_LEN)
    CypherInputStream(dirs, f, k).use { iis ->
        print(iis.read(blk2))
        print("... ")
    }

    if (ByteArrayUtils.equals(a = blk, b = blk2))
        println("Ok!")
    else
        println("Ko!")

    print("Testing encode/decode primitives... ")
    val test = "Ok!".toByteArray()
    val key = RndUtils.getRndByteArray(CryptUtils.KEY_SIZE)
    val enc = CryptUtils.encrypt(test, key)
    print(String(CryptUtils.decrypt(enc, key)))
}
